print("Un programa que calcule la hipotenusa de un triángulo. Los lados del triángulo deben ser tomados de teclado y cargados como argumentos a un método de la clase en la cual se realiza el cálculo.")
print(sep=" ")
class Triaungulo:
  def __init__(self, lado1, lado2):
    self.lado1 = lado1
    self.lado2 = lado2
    self.h= 0
  
  def calculo (self):
    import math
    lad1 = pow (self.lado1, 2)
    lad2 = pow (self.lado2, 2)
    self.cal = math.sqrt(lad1 + lad2)
    print( "La hipotenusa es : ",self.cal )

lado1=int(input("Ingrese el valor del primer lado:  "))
lado2=int(input("Ingrese el valor del segundo lado:  "))
triangulo1 = Triaungulo(lado1, lado2)

triangulo1.calculo()